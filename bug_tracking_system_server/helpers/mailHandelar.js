require('dotenv').config()
const appRoot=require('app-root-path')
const nodemailer=require('nodemailer')


module.exports.sendMail =(email,link,temp)=>{
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.EMAIL,
      pass: process.env.EMAIL_PASSWORD
    }
  });
  
var mailOptions = {
  from: 'anjanidileepkoppu@gmail.com',
  to:email,
  subject: 'Hi',
  text: 'Bye',
  html: temp
};

  return new Promise(function (resolve, reject){
     transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
          //  console.log("error: ", err);
           reject(err);
        } else {
           console.log(`Mail sent successfully!`);
           resolve(info);
        }
     });
  })
}